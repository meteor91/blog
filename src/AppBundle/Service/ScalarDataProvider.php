<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Service;
use \Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Description of ScalarDataProvider
 *
 * @author bilal
 */
class ScalarDataProvider {
    protected $em;
    public function __construct(EntityManager $entityManager) {
        $this->em = $entityManager;
    }
    
    public function commentCount($id) {

        $sql = "SELECT count(*) AS count FROM Commentary WHERE post_id=:id";
        //$dql   = "SELECT count(c) FROM AppBundle:Commentary c where";
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('count', 'count');
        $query = $this->em->createNativeQuery($sql, $rsm);
        $query->setParameter('id', $id);
        $count = $query->getSingleResult();
        return $count['count'];
    
    }
}
