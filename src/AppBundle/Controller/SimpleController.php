<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class SimpleController extends Controller
{
    /**
     * @Route("/about", name="about")
     * @Template()
     */
    public function aboutAction()
    {
        return array(
                // ...
            );    }

    /**
     * @Route("/contacts", name="contacts")
     * @Template()
     */
    public function contactsAction()
    {
        return array(
                // ...
            );    }

}
